CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(255) NOT NULL COMMENT 'Имя автора',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fio` (`fio`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='Справочник авторов';

INSERT INTO `authors` (`id`, `fio`) VALUES
	(4, 'А. В. Редькина'),
	(10, 'В. И. Иванов'),
	(3, 'Г. С. Иванова'),
	(7, 'Е. А. Новиков'),
	(2, 'Е. В. Боровская'),
	(5, 'К. В. Пушкарев'),
	(13, 'Л. И. Покидышева'),
	(9, 'Л. П. Володько'),
	(6, 'М. А. Русаков'),
	(1, 'Н. А. Давыдова'),
	(12, 'Н. В. Титовская'),
	(11, 'С. Н. Титовский'),
	(8, 'Ю. А. Шитов');

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Наименование книги',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Справочник книг';

INSERT INTO `books` (`id`, `name`) VALUES
	(5, 'Программирование: метод. указ. по контрол. работам для студентов заоч. фак.'),
	(7, 'Программирование: метод. указ. по контрол. работам № 1-3 для студентов заочн. фак.'),
	(6, 'Программирование: метод. указ. по лаб. работам №1-10 для студ. спец. 22.01'),
	(4, 'Программирование: учеб. пособие для вузов : в 2-х ч.'),
	(2, 'Программирование: учебник для вузов'),
	(1, 'Программирование: учебное пособие'),
	(3, 'Программирование: электронный учебный комплект');

CREATE TABLE IF NOT EXISTS `books_authors` (
  `book_id` int(11) NOT NULL COMMENT 'ID книги',
  `author_id` int(11) NOT NULL COMMENT 'ID автора',
  UNIQUE KEY `book_id_author_id` (`book_id`,`author_id`),
  KEY `FK_books_authors_authors` (`author_id`),
  CONSTRAINT `FK_books_authors_authors` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`),
  CONSTRAINT `FK_books_authors_books` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Авторы книг (связка авторов и книг)';

INSERT INTO `books_authors` (`book_id`, `author_id`) VALUES
	(1, 1),
	(1, 2),
	(2, 3),
	(3, 4),
	(3, 5),
	(3, 6),
	(4, 7),
	(4, 8),
	(5, 9),
	(5, 10),
	(6, 11),
	(7, 11),
	(6, 12),
	(7, 12),
	(6, 13);
