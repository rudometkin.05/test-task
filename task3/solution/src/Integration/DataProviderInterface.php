<?php

namespace solution\src\Integration;

use solution\src\DTO\RequestData;

/**
 * Интерфейс провайдера, реализующие его обязательно должны иметь метод get
 * Interface DataProviderInterface
 * @package solution\src\Integration
 */
interface DataProviderInterface
{
    /**
     * @param RequestData $request
     *
     * @return array
     */
    public function get(RequestData $request): array;
}