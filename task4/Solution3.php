<?php

class Solution3
{
    use Valid;

    /**
     * Наиболее быстрое решение, которого удалось добиться
     * @param $a
     * @param $b
     * @return string
     */
    public static function sum(string $a, string $b): string {
        self::validate($a);
        self::validate($b);

        $maxLength = 8;

        $overNum   = 10 ** $maxLength;
        $padPredel = 10 ** ($maxLength - 1);

        $sizeA = strlen($a);
        $sizeB = strlen($b);

        $remember = 0;
        $result   = '';

        while ($sizeA > 0 || $sizeB > 0) {
            if ($sizeA > 0) {
                $digitsCount = $sizeA >= $maxLength ? $maxLength : $sizeA;
                $sizeA       = $sizeA > $maxLength ? $sizeA - $maxLength : 0;
                $partA       = (int) substr($a, $sizeA, $digitsCount);
            } else {
                $partA = 0;
            }

            if ($sizeB > 0) {
                $digitsCount = $sizeB >= $maxLength ? $maxLength : $sizeB;
                $sizeB       = $sizeB > $maxLength ? $sizeB - $maxLength : 0;
                $partB       = (int) substr($b, $sizeB, $digitsCount);
            } else {
                $partB = 0;
            }

            $sum = $partA + $partB + $remember;
            if ($sum >= $overNum) {
                $sum      -= $overNum;
                $remember = 1;
            } else {
                $remember = 0;
            }

            if ($sum < $padPredel) {
                $sum = str_pad($sum, $maxLength, '0', STR_PAD_LEFT);
            }

            $result = $sum . $result;
        }

        if ($remember) {
            $result = '1' . $result;
        }

        return ltrim($result, '0') ?: '0';
    }
}