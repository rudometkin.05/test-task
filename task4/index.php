<?php
/* Простая проверка результатов */

include_once 'Valid.php';

include_once 'Solution1.php';
include_once 'Solution2.php';
include_once 'Solution3.php';

$tests = [
    [
        'in'  => ['975824502840890274124606140614241234567890123456789012345678901234567890', '958694089458963490674906092469090242467990249624962490624096249624962496'],
        'out' => '1934518592299853764799512233083331477035880373081751502969775150859530386',
    ],
    [
        'in'  => ['100', '900'],
        'out' => '1000',
    ],
    [
        'in'  => ['0', '0'],
        'out' => '0',
    ],
    [
        'in'  => ['1', '1'],
        'out' => '2',
    ],
    [
        'in'  => ['155', '55'],
        'out' => '210',
    ],
    [
        'in'  => ['1x55', '55'],
        'out' => InvalidArgumentException::class,
    ],
    [
        'in'  => ['10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000055', '55'],
        'out' => '10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110',
    ],
    [
        'in'  => ['155', '55000000000000000000000000000000000000000000000000000000000000000000000000005'],
        'out' => '55000000000000000000000000000000000000000000000000000000000000000000000000160',
    ],
    [
        'in'  => ['22222222222222222222222222222222222222222222222222222', '999999999999999999999999999999999919'],
        'out' => '22222222222222223222222222222222222222222222222222141',
    ],
];

$solutions = [
    [
        'name'   => 'Через массивы',
        'class'  => Solution1::class,
        'method' => 'sum',
    ],
    [
        'name'   => 'Через строки',
        'class'  => Solution2::class,
        'method' => 'sum',
    ],
    [
        'name'   => 'Самый быстрый',
        'class'  => Solution3::class,
        'method' => 'sum',
    ],
];

$results = [];
foreach ($tests as $test) {
    foreach ($solutions as $solution) {
        $result     = '';
        $class      = $solution['class'];
        $method     = $solution['method'];
        $parameters = $test['in'];
        $message    = '';

        $t1 = microtime(true);
        try {
            $result = $class::$method(...$parameters);
        } catch (Exception $ex) {
            if (class_exists($test['out']) && $ex instanceof $test['out']) {
                $result  = $test['out'];
                $message = "Correct exception {$ex->getMessage()}";
            } else {
                $result  = null;
                $message = "Unexpected exception: {$ex->getMessage()}";
            }
        }
        $t2 = microtime(true);

        $results[] = [
            'title'    => $solution['name'],
            'result'   => $result === $test['out'],
            'time'     => $t2 - $t1,
            'message'  => $message,
            'expected' => $test['out'],
            'actual'   => $result,
            'data'     => $test['in'],
        ];
    }
}

foreach ($results as $result):
?>
[<?= $result['title']; ?>]
    Параметры: <?= implode(', ', $result['data']) . PHP_EOL; ?>
    Результат: <?php echo ($result['result'] ? 'Верно' : 'Ошибка') . PHP_EOL; ?>
<?php if (!empty($result['message'])): ?>
    Сообщение: <?= $result['message'] . PHP_EOL ?>
<?php endif; ?>
<?php if (!$result['result']): ?>
    Ожидание: <?= $result['expected'] . PHP_EOL ?>
    Реальность: <?= $result['actual'] . PHP_EOL ?>
<?php endif; ?>
    Время:     <?= number_format($result['time'], 8) . PHP_EOL . PHP_EOL ?>

<?php
endforeach;
?>